package com.example.pramod_d.gyroscope_movement;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.Image;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by pramod_d on 25/2/2016.
 */
public class DrawView extends View {
    float orientationRoll;
    float rollAngle;
    float picthAngle;
    float azumitAngle;
    float orientationPitch;
    float orientationazumit;
    Bitmap background;
    Image back;
    public DrawView(Context context,View container){
        super(context);
        background= BitmapFactory.decodeResource(context.getResources(), R.drawable.koala);
        //orientationPitch=(container.getHeight()/2)-(background.getHeight()/2);
       // orientationRoll=(container.getWidth()/2)-(background.getWidth()/2);

        invalidate();

    }
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        canvas.drawBitmap(background,orientationazumit,0, null);
    }
    public void move(float rollAngle,float offsetWidth){
        this.rollAngle=rollAngle;
        this.picthAngle=picthAngle;
        //orientationPitch=picthAngle*(background.getHeight()/45)-background.getHeight()/2 + offsetHeight;
        orientationRoll=rollAngle*(background.getWidth()/180)-background.getWidth()/2 + offsetWidth;
        invalidate();
    }
    public void move_pitch(float picthAngle,float offsetHeight){
        this.picthAngle=picthAngle;
        orientationPitch=picthAngle*(background.getHeight()/45)-background.getHeight()/2 + offsetHeight;
        invalidate();
    }
    public void move_azumit(float azumitAngle ){
        this.azumitAngle=azumitAngle;
        orientationazumit=azumitAngle*(background.getWidth()/180);
        invalidate();
    }

}
