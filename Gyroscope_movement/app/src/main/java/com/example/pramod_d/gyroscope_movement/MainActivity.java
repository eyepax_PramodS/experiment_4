package com.example.pramod_d.gyroscope_movement;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements SensorEventListener{
    DrawView drawView;
    LinearLayout drawViewLayout;
    private SensorManager mSensorManager;
    Sensor accelerometer;
    Sensor magnetometer;
    float[] mGravity;
    float[] mGeomagnetic;
    private TextView xView,yView,zView;
    private float orientationValues[]=new float[3];
    ArrayList<Float> orientationValueList =new ArrayList<Float>();
    ArrayList<Float> pitchValueList =new ArrayList<Float>();
    ArrayList<Float> azumitValueList =new ArrayList<Float>();
    float Threshold=1f;
    float thresholdPitch=1f;
    float thresholdAzumit=2f;
    float pitchOffset=0;
    float rotationOffset=0;
    boolean firstValue=false;
    Button resetButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        xView=(TextView)findViewById(R.id.textView);
        yView=(TextView)findViewById(R.id.textView3);
        zView=(TextView)findViewById(R.id.textView4);
        resetButton=(Button)findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstValue=false;
            }
        });
        drawViewLayout=(LinearLayout)findViewById(R.id.drawViewLayout);
        drawView=new DrawView(this,drawViewLayout);
        drawViewLayout.addView(drawView);


    }
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {


        xView.setText("Az:"+orientationValues[0]);
        yView.setText("Pi:"+orientationValues[1]);
        zView.setText("Ro:" + orientationValues[2]);

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                orientationValues[0]=orientation[0]*(180/(float)Math.PI);
                orientationValues[1]=orientation[1]*(180/(float)Math.PI);
                orientationValues[2]=orientation[2]*(180/(float)Math.PI);

                orientationValueList.add(orientation[2]*(180/(float)Math.PI));
                pitchValueList.add(orientation[1]*(180/(float)Math.PI));
                azumitValueList.add(orientation[0]*(180/(float)Math.PI));
                System.out.println("orientaion values" + orientation);

                // orientation contains: azimut, pitch and roll
                if(!firstValue){
                    pitchOffset=-orientationValues[1];
                    rotationOffset=-orientationValues[2];
                    firstValue=true;
                }
            }
            if(orientationValueList.size()==3){
                orientationValueList.remove(0);
            }
            if(pitchValueList.size()==3){
                pitchValueList.remove(0);
            }
            if(azumitValueList.size()==3){
                azumitValueList.remove(0);
            }
        }
        //drawView.move_azumit(orientationValues[0]);
        //drawView.move_pitch(orientationValues[1]+pitchOffset,drawViewLayout.getWidth()/2);
       // drawView.move(orientationValues[2]+rotationOffset,drawViewLayout.getHeight()/2);
//        try{
//            drawView.move(orientationValues[2]);
//        }
//        catch(Exception e){
//            System.out.println("--");
//        }


//        for (int i = -180; i <=175 ; i=i+3) {
//            if(i< orientationValues[2] &&orientationValues[2]< i+2){
//                drawView.move(i*1f);
//            }
//        }

////        //bitmap slider
//       try{
//           if(orientationValueList.get(0)>0&&orientationValueList.get(1)>0){
//               if(Math.abs(orientationValueList.get(0) - orientationValueList.get(1))>Threshold)
//                   drawView.move(orientationValueList.get(1) + rotationOffset, drawViewLayout.getHeight() / 2);
//           }
//           else if(orientationValueList.get(0)<0&&orientationValueList.get(1)<0){
//               if(Math.abs(orientationValueList.get(0) - orientationValueList.get(1))>Threshold)
//                     drawView.move(orientationValueList.get(1) + rotationOffset, drawViewLayout.getHeight() / 2);
//           }
//           else if(orientationValueList.get(0)>0&&orientationValueList.get(1)<0){
//               if(Math.abs(orientationValueList.get(0) +orientationValueList.get(1))>Threshold)
//                     drawView.move(orientationValueList.get(1) + rotationOffset, drawViewLayout.getHeight() / 2);
//           }
//           else if(orientationValueList.get(0)<0&&orientationValueList.get(1)>0){
//               if(Math.abs(orientationValueList.get(0) +orientationValueList.get(1))>Threshold)
//                    drawView.move(orientationValueList.get(1) + rotationOffset, drawViewLayout.getHeight() / 2);
//           }
//       }
//       catch (Exception e) {
//           System.out.println("Error");
//       }
//        try{
//            if(pitchValueList.get(0)>0&&pitchValueList.get(1)>0){
//                if(Math.abs(pitchValueList.get(0) - pitchValueList.get(1))>thresholdPitch)
//                    drawView.move_pitch(pitchValueList.get(1) + pitchOffset, drawViewLayout.getWidth() / 2);
//            }
//            else if(pitchValueList.get(0)<0&&pitchValueList.get(1)<0){
//                if(Math.abs(pitchValueList.get(0) - pitchValueList.get(1))>thresholdPitch)
//                    drawView.move_pitch(pitchValueList.get(1) + pitchOffset, drawViewLayout.getWidth() / 2);
//            }
//            else if(pitchValueList.get(0)>0&&pitchValueList.get(1)<0){
//                if(Math.abs(pitchValueList.get(0) +pitchValueList.get(1))>thresholdPitch)
//                    drawView.move_pitch(pitchValueList.get(1) + pitchOffset, drawViewLayout.getWidth() / 2);
//            }
//            else if(pitchValueList.get(0)<0&&pitchValueList.get(1)>0){
//                if(Math.abs(pitchValueList.get(0) +pitchValueList.get(1))>thresholdPitch)
//                    drawView.move_pitch(pitchValueList.get(1) + pitchOffset, drawViewLayout.getWidth() / 2);
//            }
//        }
//        catch (Exception e) {
//            System.out.println("Error");
//        }
        try{
            if(azumitValueList.get(0)>0&&azumitValueList.get(1)>0||(azumitValueList.get(0)<0&&azumitValueList.get(1)<0)){
                if(Math.abs(azumitValueList.get(0)-azumitValueList.get(1))>thresholdAzumit)
                    drawView.move_azumit(azumitValueList.get(1));
            }
            if(azumitValueList.get(0)>0&&azumitValueList.get(1)<0||(azumitValueList.get(0)<0&&azumitValueList.get(1)>0)){
                if(Math.abs(azumitValueList.get(0)+azumitValueList.get(1))>thresholdAzumit)
                    drawView.move_azumit(azumitValueList.get(1));
            }
        }
        catch (Exception e) {
            System.out.println("Error");
        }



    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }
}
